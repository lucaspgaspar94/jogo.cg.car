/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BaseGame;

/**
 *
 * @author Lucas Gaspar
 */
public class Move {
    private int _dirX = 0;
    private int _dirY = 0;
    private int _velX = 1;
    private int _velY = 1;
    
    private Transform _transform;

    public int getDirX() {
        return _dirX;
    }

    public void setDirX(int _dirX) {
        this._dirX = _dirX;
    }

    public int getDirY() {
        return _dirY;
    }

    public void setDirY(int _dirY) {
        this._dirY = _dirY;
    }

    public int getVelX() {
        return _velX;
    }

    public void setVelX(int _velX) {
        this._velX = _velX;
    }

    public int getVelY() {
        return _velY;
    }

    public void setVelY(int _velY) {
        this._velY = _velY;
    }

    public Move(Transform _transform) {
        this._transform = _transform;
    }

    public void setVelocity(int vel){
        this._velX = vel;
        this._velY = vel;
    }
    
    public void Move(int dirX, int dirY, int velX, int velY){
        this._dirX = dirX;
        this._dirY = dirY;
        this._velX = velX;
        this._velY = velY;
        
        _transform.setPositionX(_transform.getPositionX() + (dirX * velX));
        _transform.setPositionY(_transform.getPositionY() + (dirY * velY));
    }
    
    public void Move(int dirX, int dirY){
        this.Move(dirX, dirY, this._velX, this._velY);
    }
    
    public void Move(){
        this.Move(this._dirX, this._dirY, this._velX, this._velY);
    }
    
    public void MoveRight(){
        this.Move(1, 0);
    }
    
    public void MoveLeft(){
        this.Move(-1, 0);
    }
    
    public void MoveUp(){
        this.Move(0, -1);
    }
    
    public void MoveDown(){
        this.Move(0, 1);
    }
    
    public void Stop(){
        this.Move(0, 0);
    }
}
