/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BaseGame;

import java.awt.List;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Lucas Gaspar
 */
public class CollisionConfiguration {

    Map<String, ArrayList<String>> _collisions;
    ArrayList<String> _allTags;

    public void AddTag(String tag) {
        if (_allTags == null) {
            _allTags = new ArrayList<>();
        }

        if (_collisions == null) {
            _collisions = new HashMap<>();
        }

        if (_allTags.contains(tag)) {
            return;
        }

        _allTags.add(tag);
        _collisions.put(tag, _allTags);
    }

    public void RemoveCollision(String tag, String exclude) {
        Removing(tag, exclude);
        Removing(exclude, tag);
    }
    
    private void Removing(String tag, String exclude){
        ArrayList<String> collisionsTag = _collisions.get(tag);
        if(collisionsTag.contains(exclude)){
            collisionsTag.remove(exclude);
            _collisions.put(tag, collisionsTag);
        }
    }

    public boolean HasCollision(String tag, String colliderTag) {
        return CheckCollision(tag, colliderTag) || CheckCollision(colliderTag, tag);
    }
    
    private boolean CheckCollision(String tag, String collisionTag){
        ArrayList<String> collisionsTag = _collisions.get(tag);
        if(collisionsTag.contains(collisionTag))
            return true;
        
        return false;
    }

}
