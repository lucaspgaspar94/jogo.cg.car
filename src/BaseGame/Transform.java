/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BaseGame;

/**
 *
 * @author Lucas Gaspar
 */
public class Transform {
    
    private int _sizeX;
    private int _sizeY;
    
    private int _positionX;
    private int _positionY;  

    public Transform(int _sizeX, int _sizeY, int _positionX, int _positionY) {
        this._sizeX = _sizeX;
        this._sizeY = _sizeY;
        this._positionX = _positionX;
        this._positionY = _positionY;
    }
   
    public int getSizeX() {
        return _sizeX;
    }

    public void setSizeX(int _sizeX) {
        this._sizeX = _sizeX;
    }

    public int getSizeY() {
        return _sizeY;
    }

    public void setSizeY(int _sizeY) {
        this._sizeY = _sizeY;
    }

    public int getPositionX() {
        return _positionX;
    }

    public void setPositionX(int _positionX) {
        this._positionX = _positionX;
    }

    public int getPositionY() {
        return _positionY;
    }

    public void setPositionY(int _positionY) {
        this._positionY = _positionY;
    }
    
    public int getLeft(){
        return _positionX;
    }   
    
    public int getRight(){
        return _positionX + _sizeX;
    }
    
    public int getTop(){
        return _positionY;
    }
    
    public int getBottom(){
        return _positionY + _sizeY;
    }
    
    
       
    
}
