/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BaseGame;

import Game.ActionEnum;
import Game.Game;
import java.awt.Graphics;

/**
 *
 * @author Lucas Gaspar
 */
public abstract class Behavior {

    protected String Tag;
    protected CollisionConfiguration _collisionConfiguration;
    protected Collider _collider;
    protected Transform _transform;
    protected Move _move;
    protected Game _game;
    private int Id;

    public int getId() {
        return Id;
    }
    
    

    public Collider getCollider() {
        return _collider;
    }

    public Transform getTransform() {
        return _transform;
    }

    public Move getMove() {
        return _move;
    }
    
    public boolean hasCollider(){
        return _collider != null;
    }
    
    public boolean hasTransform(){
        return _transform != null;
    }
    
    public abstract void draw (Graphics g);
    
    public abstract void update();
    
    public abstract void action(ActionEnum action);
    
    public abstract void start();

    public void Build(int id ,String Tag, CollisionConfiguration _collisionConfiguration, Collider _collider, Transform _transform, Move _move, Game _game) {
        this.Id = id;
        this.Tag = Tag;
        this._collisionConfiguration = _collisionConfiguration;
        this._collider = _collider;
        this._transform = _transform;
        this._move = _move;        
        this._game = _game;
        this._collisionConfiguration.AddTag(Tag);
        this.start();
    }
        
    public String getTag() {
        return Tag;
    }

    public CollisionConfiguration getCollisionConfiguration() {
        return _collisionConfiguration;
    }
        
}
