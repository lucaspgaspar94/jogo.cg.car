/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BaseGame;

import java.util.ArrayList;

/**
 *
 * @author Lucas Gaspar
 */
public class Collider {

    private Transform _transform;
    private Behavior _behavior;
    private ICollider _object;
    
    public Behavior getBehavior() {
        return _behavior;
    }

    public Transform getTransform() {
        return _transform;
    }

    public ICollider getObject() {
        return _object;
    }

    public Collider(Transform _transform, Behavior _behavior, ICollider _object) {
        this._transform = _transform;
        this._behavior = _behavior;
        this._object = _object;
    }
    
    private void CheckCollision(Collider c) {          
        if (this != c && _behavior.getCollisionConfiguration().HasCollision(_behavior.getTag(), c.getBehavior().getTag()) && CheckIfHasCollision(c.getTransform())) 
        {
           _object.Collid(c);
        }
    }
    
    private boolean CheckIfHasCollision(Transform t){
        
        Transform me = this._transform;
        
        if(me.getRight() >= t.getRight() && me.getLeft() >= t.getRight())
            return false;
        
        if(me.getLeft() <= t.getLeft() && me.getRight() <= t.getLeft())
            return false;
        
        if(me.getTop() <= t.getTop() && me.getBottom() <= t.getTop())
            return false;
        
        if(me.getBottom() >= t.getBottom() && me.getTop() >= t.getBottom())
            return false;
        
        return true;
    }

    public void CheckCollision(ArrayList<Behavior> behaviors) {
        for(Behavior b : behaviors){
            if(b.hasCollider())
                CheckCollision(b.getCollider());
        }
    }

}
