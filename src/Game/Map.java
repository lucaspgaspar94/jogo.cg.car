/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Game;

import BaseGame.Behavior;
import BaseGame.CollisionConfiguration;
import java.awt.Color;
import java.awt.Graphics;
import java.util.Random;

/**
 *
 * @author Lucas Gaspar
 */
public class Map extends Behavior {

    private int _velocity;
    private int _roadSize;

    private int _percentGas;
    private int _maxItens;
    private int _itensCount;

    private int _timeItens;
    public Map(int velocity, Game game, int roadSize, int percentGas, int maxItens, CollisionConfiguration colConfig) {
        this._velocity = velocity;
        this._roadSize = roadSize;
        _percentGas = percentGas;
        _maxItens = maxItens;

        super.Build(game.GetIdBehaivor(), "Map", colConfig, null, null, null, game);
    }

    @Override
    public void start() {
        _timeItens = 0;
        _itensCount = 0;
    }

    @Override
    public void draw(Graphics g) {
        int sizeRoad = calculateSizeRoad(this._game.getWidth());
        int margin = calculateMargin();

        g.setColor(Color.GREEN);
        g.fillRect(0, 0, _game.getWidth(), _game.getHeight());

        g.setColor(Color.GRAY);
        g.fillRect(margin, 0, sizeRoad, _game.getHeight());
    }

    private int calculateSizeRoad(int width) {
        int sizeRoad = width * this._roadSize / 100;
        return sizeRoad;
    }

    private int calculateMargin() {
        int width = this._game.getWidth();
        int sizeRoad = calculateSizeRoad(width);
        int margin = (width - sizeRoad) / 2;
        return margin;
    }

    public int LimitRight() {
        return this._game.getWidth() - calculateMargin();
    }

    public int LimitLeft() {
        return calculateMargin();
    }

    @Override
    public void update() {

        if (_game.getStatus() == GameStatus.Playing) {          
            CalculateNewItem();
        }
    }

    private int GetPositionX() {
        int diference = LimitRight() - LimitLeft() - 50;
        Random r = new Random();
        return r.nextInt(diference) + LimitLeft();
    }

    private void CalculateNewItem() {

        if (_timeItens < 250) {
            _timeItens+= this._game.GetVelocityMultiply();
            return;
        }

        _timeItens = 0;

        if (_itensCount < _maxItens) {

            Random r = new Random();

            if (r.nextInt(100) > 30) {
                _itensCount ++;
                if (r.nextInt(100) <= _percentGas) {
                    //CREATE GAS
                    Gas g = new Gas(_collisionConfiguration, _game, GetPositionX(), this);
                    _game.AddBehaivor(g);
                } else {
                    //CREATE SPIRE
                    Spire s = new Spire(_collisionConfiguration, _game, GetPositionX(), this);
                    _game.AddBehaivor(s);
                    
                }
            }
        }

    }

    @Override
    public void action(ActionEnum action) {
    }  
    
    public void RemoveItem(Behavior b){
        _itensCount--;
        _game.RemoveBehaivor(b);
    }

}
