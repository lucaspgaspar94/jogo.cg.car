/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Game;

import BaseGame.Behavior;
import BaseGame.Collider;
import BaseGame.CollisionConfiguration;
import BaseGame.ICollider;
import BaseGame.Move;
import BaseGame.Transform;
import java.awt.Color;
import java.awt.Graphics;
import javax.swing.ImageIcon;

/**
 *
 * @author Lucas Gaspar
 */
public class Spire extends Behavior implements ICollider {

    private final Map _map;
    private ImageIcon _image;

    public Spire(CollisionConfiguration colConfig, Game game, int startX, Map map) {

        int sizeX = 50;
        int sizeY = 50;

        Transform t = new Transform(sizeX, sizeY, startX, -sizeY - 10);

        Collider c = new Collider(t, this, this);

        Move m = new Move(t);

        _map = map;

        super.Build(game.GetIdBehaivor(), "Spire", colConfig, c, t, m, game);

        m.MoveDown();
        _image = new ImageIcon(this.getClass().getResource("/Resources/spire.png"));

    }

    @Override
    public void draw(Graphics g) {
        if (_image != null) {
            g.drawImage(_image.getImage(), _transform.getPositionX(), _transform.getPositionY(), _transform.getSizeX(), _transform.getSizeY(), null);
        } else {
            g.setColor(Color.RED);
            g.fillOval(_transform.getPositionX(), _transform.getPositionY(), _transform.getSizeX(), _transform.getSizeY());
        }
    }

    @Override
    public void update() {

        if (this._game.getStatus() == GameStatus.Playing) {
            _move.Move();
            draw(_game.getGraphics());
            if (_transform.getPositionY() > _game.getHeight() + _transform.getSizeY()) {
                _map.RemoveItem(this);
            }

            this._move.setVelY(this._game.GetVelocityMultiply());
        }
    }

    @Override
    public void action(ActionEnum action) {

    }

    @Override
    public void start() {

    }

    @Override
    public void Collid(Collider c) {

    }

}
