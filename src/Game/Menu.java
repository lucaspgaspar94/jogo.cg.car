/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Game;

import BaseGame.Behavior;
import BaseGame.CollisionConfiguration;
import BaseGame.Transform;
import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author Lucas Gaspar
 */
public class Menu extends Behavior {

    public Menu(CollisionConfiguration colConfig, Game game) {

        super.Build(game.GetIdBehaivor(), "Menu", colConfig, null, null, null, game);

    }

    @Override
    public void draw(Graphics g) {
        g.setColor(Color.BLACK);
        if (_game.getStatus() == GameStatus.Menu) {
            g.drawString("Aperte ESPAÇO para inicial", _game.getWidth() / 2, _game.getHeight() / 2);
        }
    }

    @Override
    public void update() {

    }

    @Override
    public void action(ActionEnum action) {

        if (action == ActionEnum.Space) {
            if (_game.getStatus() == GameStatus.Menu) {
                _game.StartGame();
            }
        }

    }

    @Override
    public void start() {

    }

}
