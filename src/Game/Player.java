/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Game;

import BaseGame.Behavior;
import BaseGame.Collider;
import BaseGame.CollisionConfiguration;
import BaseGame.ICollider;
import BaseGame.Move;
import BaseGame.Transform;
import java.awt.Color;
import java.awt.Graphics;
import java.io.IOException;
import javax.swing.ImageIcon;

/**
 *
 * @author Lucas Gaspar
 */
public class Player extends Behavior implements ICollider {

    private int _lifes;
    private PlayerStatus _status;
    private Map _map;
    private ImageIcon _image;
    private double _gas;
    private double _multipleGasConsum;

    private int _maxLifes;

    public Player(CollisionConfiguration config, int _lifes, Game game, Map map) {

        this._game = game;

        this._maxLifes = _lifes;

        this._gas = 100;
        this._multipleGasConsum = 0.01;

        int width = game.getWidth();

        int sizeX = (int) (width * 0.05f);
        int sizeY = (int) (width * 0.1f);

        int startX = getStartPositionX();
        int startY = getStartPositionY();

        Transform t = new Transform(sizeX, sizeY, startX, startY);
        Collider c = new Collider(t, this, this);
        Move m = new Move(t);
        this.Build(game.GetIdBehaivor(), "Player", config, c, t, m, game);

        this._map = map;
        _image = new ImageIcon(this.getClass().getResource("/Resources/car_topview.png"));
    }

    private int getStartPositionX() {
        int width = _game.getWidth();
        int sizeX = (int) (width * 0.05f);
        return width / 2 - sizeX / 2;
    }

    private int getStartPositionY() {
        int width = _game.getWidth();
        int height = _game.getHeight();
        int sizeY = (int) (width * 0.1f);
        return height - sizeY - (int) (height * 0.1f);
    }

    @Override
    public void start() {
        this._transform.setPositionX(getStartPositionX());
        this._transform.setPositionY(getStartPositionY());
        this._status = PlayerStatus.Running;
        this._lifes = this._maxLifes;
    }

    @Override
    public void draw(Graphics g) {

        if (_game.getStatus() == GameStatus.Playing) {
            g.setColor(Color.BLACK);
            String s = ("Gasolina: " + (int) this._gas);
            g.drawString(s, 80, 80);
            s = ("Status veículo: " + (int) (this._lifes * 100 / this._maxLifes));
            g.drawString(s, 80, 100);

        }

        if (this._status == PlayerStatus.Hit) {
            drawHit(g);
            return;
        }
        drawRacing(g);

    }

    private void drawRacing(Graphics g) {

        if (_image != null) {
            g.drawImage(_image.getImage(), _transform.getPositionX(), _transform.getPositionY(), _transform.getSizeX(), _transform.getSizeY(), null);
        } else {
            g.setColor(Color.BLUE);
            g.fillOval(_transform.getPositionX(), _transform.getPositionY(), _transform.getSizeX(), _transform.getSizeY());
        }

    }

    private int _countHit = 0;

    private void drawHit(Graphics g) {
        if (_visible) {
            drawRacing(g);
        }
    }

    @Override
    public void Collid(Collider c) {

        if (this._status == PlayerStatus.Running) {

            if (c.getBehavior().getTag().equals("Gas")) {
                this._gas += 20;
                if (this._gas > 100) {
                    this._gas = 100;
                }
            }
            if (c.getBehavior().getTag().equals("Spire")) {
                this._lifes--;
                if (this._lifes <= 0) {
                    GameOver();
                    return;
                }
                this._status = PlayerStatus.Hit;
            }
            this._map.RemoveItem(c.getBehavior());
        }

    }

    private boolean _visible = true;

    private void GameOver() {

        this._status = PlayerStatus.Broke;
        this._game.GameOver();
    }

    @Override
    public void update() {

        if (this._game.getStatus() == GameStatus.Playing) {
            if (this._status != PlayerStatus.Broke) {
                this._move.Move();
                HitEffect();
                GasUpdate();
            }
        }
    }

    private void GasUpdate() {
        if (this._gas <= 0) {
            this._status = PlayerStatus.Broke;
            GameOver();
            return;
        }
        this._gas -= this._multipleGasConsum;
    }

    private void HitEffect() {
        if (this._status == PlayerStatus.Hit) {
            this._countHit++;
            if (this._countHit % 10 == 0) {
                this._visible = !this._visible;
            }
            if (this._countHit > 300) {
                this._status = PlayerStatus.Running;
                this._countHit = 0;
            }
        }
    }

    @Override
    public void action(ActionEnum action) {
        if (_game.getStatus() == GameStatus.Playing && this._status != PlayerStatus.Broke) {
            switch (action) {
                case None:
                    this._move.Stop();
                    break;
                case Arrow_Down:
                    this._game.SpeedDown();
                    this._move.setVelX(this._game.GetVelocityMultiply());
                    this._multipleGasConsum = 0.01;
                    break;
                case Arrow_Up:
                    this._game.SpeedUp();
                    this._move.setVelX(this._game.GetVelocityMultiply());
                    this._multipleGasConsum = 0.02;
                    break;
                case Arrow_Left:
                    if (this._transform.getLeft() - 1 > this._map.LimitLeft()) {
                        this._move.MoveLeft();
                    } else {
                        this._move.Stop();
                    }
                    break;
                case Arrow_Right:
                    if (this._transform.getRight() + 1 < this._map.LimitRight()) {
                        this._move.MoveRight();
                    } else {
                        this._move.Stop();
                    }
                    break;
            }
        }
    }

}
